package io.ds.echo;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

public class UDPEchoServer {
  public static void main(String args[]) {
    
    try (DatagramSocket aSocket = new DatagramSocket(6789)){
      System.out.println("Starting echo server ...");
      byte[] buffer = new byte[1000];
      while (true) {
        DatagramPacket request = new DatagramPacket(buffer, buffer.length);
        System.out.println("Waiting for requests!");
        aSocket.receive(request);
        System.out.println("Received request!");
        DatagramPacket reply = new DatagramPacket(request.getData(), request.getLength(),
            request.getAddress(), request.getPort());
        aSocket.send(reply);
      }
    } catch (SocketException e) {
      System.out.println("Socket: " + e.getMessage());
    } catch (IOException e) {
      System.out.println("IO: " + e.getMessage());
    } 
  }
}
